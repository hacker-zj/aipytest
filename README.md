# API接口自动化测试平台

这是一个基于 `pytest`、`yaml`、`allure`、`jenkins` 以及 邮箱/钉钉群通知 开发的API接口自动化测试平台,
项目采用0代码热加载，使用者不需要任何代码基础

## 目录结构

```
aipytest
├── .pytest_cache        # pytest缓存文件
├── allure-report        # allure生成的报告文件
├── allure-results       # allure测试结果文件
├── base                 # 基础类和方法
├── common               # 公共方法和工具类
├── conf                 # 配置文件
├── data                 # 测试数据
├── log                  # 日志文件
├── mockdata             # 模拟数据
├── report               # 测试报告
├── testcase             # 测试用例
├── venv                 # 虚拟环境
├── .gitignore           # git忽略文件
├── conftest.py          # pytest配置文件
├── environment.xml      # 环境配置文件
├── extract.yaml         # 提取的yaml文件
├── LICENSE              # 许可证
├── pytest.ini           # pytest配置文件
├── requirements.txt     # 依赖包列表
└── run.py               # 运行脚本
```

## 安装依赖

在开始使用之前，请确保已经安装了必要的依赖包。你可以通过以下命令安装：

```bash
pip install -r requirements.txt
```

## 运行测试

你可以通过以下命令运行测试：

```bash
pytest
```

## 生成测试报告

测试完成后，可以生成 `allure` 报告：

```bash
allure generate ./allure-results -o ./allure-report --clean
```

然后，可以通过以下命令查看报告：

```bash
allure open ./allure-report
```

## 持续集成

本项目支持 `Jenkins` 持续集成。你可以在 `Jenkins` 中配置以下步骤来运行测试和生成报告：

1. 拉取代码仓库
2. 安装依赖
3. 运行测试
4. 生成并发布 `allure` 报告

## 通知

测试完成后，可以通过邮箱或钉钉群通知相关人员。你可以在 `conf` 目录下的配置文件中设置通知的相关信息。

## 贡献

欢迎大家贡献代码！你可以通过提交 `Pull Request` 的方式来贡献你的代码。

## 许可证

本项目采用 MIT 许可证。详情请参阅 `LICENSE` 文件。

## 联系方式

如果你有任何问题或建议，请通过以下方式联系我们：

- 邮箱：2696675997@qq.com
-微信：hacker-zj

感谢你使用我们的API接口自动化测试平台！