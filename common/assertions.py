from common.recordlog import logs
import allure

def assert_response(assertions, response, status_code):
    """
    根据断言列表对响应结果进行断言操作并记录日志

    :param assertions: 断言列表，例如 [{'contains': {'msg': '登录成功'}}, {'eq': {'msg_code': 200}}, {'noteq': {'msg': '失败'}}]
    :param response: 响应结果字典，例如 {"error_code": null, "msg": "登录成功", "msg_code": 200, "orgId": "4140913758110176843", "token": "c7A8b483787c9384c0032CeBedcf3", "userId": "1359701149321159089"}
    :param status_code: 响应状态码，例如 200
    :return: 断言结果，True 表示所有断言通过，False 表示有断言失败
    """
    try:
        # 检查状态码
        if status_code != 200:
            logs.info(f"断言失败: 接口状态码是 {status_code}，预期状态码是 200")
            allure.attach(f"断言失败: 接口状态码是 {status_code}，预期状态码是 200", '状态码断言结果')
            return False

        for assertion in assertions:
            for key, condition in assertion.items():
                for field, expected_value in condition.items():
                    if field not in response:
                        condition_value = f"断言失败: 当前断言字段是 {field}，接口实际响应结果是 '无此字段'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                        logs.info(condition_value)
                        allure.attach(condition_value, '字段存在性断言结果')
                        return False

                    actual_value = response[field]

                    if key == 'contains':
                        if expected_value not in actual_value:
                            condition_value = f"断言失败: 当前断言字段是 {field}，接口实际响应结果是 '{actual_value}'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                            logs.info(condition_value)
                            allure.attach(condition_value, '包含断言结果')
                            return False
                        else:
                            condition_value = f"断言成功: 当前断言字段是 {field}，接口实际响应结果是 '{actual_value}'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                            logs.info(condition_value)
                            allure.attach(condition_value, '包含断言结果')

                    elif key == 'eq':
                        if actual_value != expected_value:
                            condition_value = f"断言失败: 当前断言字段是 {field}，接口实际响应结果是 '{actual_value}'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                            logs.info(condition_value)
                            allure.attach(condition_value, '等于断言结果')
                            return False
                        else:
                            condition_value = f"断言成功: 当前断言字段是 {field}，接口实际响应结果是 '{actual_value}'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                            logs.info(condition_value)
                            allure.attach(condition_value, '等于断言结果')

                    elif key == 'noteq':
                        if actual_value == expected_value:
                            condition_value = f"断言失败: 当前断言字段是 {field}，接口实际响应结果是 '{actual_value}'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                            logs.info(condition_value)
                            allure.attach(condition_value, '不等于断言结果')
                            return False
                        else:
                            condition_value = f"断言成功: 当前断言字段是 {field}，接口实际响应结果是 '{actual_value}'，YAML 文件断言结果是 {expected_value}，采用的是 {key} 断言模式"
                            logs.info(condition_value)
                            allure.attach(condition_value, '不等于断言结果')

                    else:
                        condition_value = f"未知的断言类型: {key}，接口实际响应结果是 {response}，YAML 文件断言结果是 {assertion}"
                        logs.info(condition_value)
                        allure.attach(condition_value, '未知断言类型结果')
                        return False

        return True

    except Exception as e:
        error_message = f"断言过程中出现异常: {str(e)}"
        logs.error(error_message)
        allure.attach(error_message, '断言异常结果')
        return False