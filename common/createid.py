def get_module_id():
    for i in range(1, 1000):
        module_id = 'M' + str(i).zfill(2) + '_'
        yield module_id


def get_case_id():
    for i in range(1, 10000):
        case_id = 'C' + str(i).zfill(2) + '_'  # 4位数字
        yield case_id


m_id = get_module_id()
c_id = get_case_id()
