import random
import sys
import time

sys.path.append(".")
# from readyaml import get_extract_data as get_write
from .readyaml import get_extract_data as get_write
from .decode import CryptoUtils


class DebugTalk(CryptoUtils):

    def __init__(self):
        self.read = get_write

    def get_extract_data(self, key_name, randoms=None):
        """
        从数据库中读取指定键名key_name对应的数据，并根据参数randoms对数据进行提取。
        
        Args:
            key_name (str): 指定的键名。
            randoms (Union[None, int], optional): 提取数据的规则，可选值为None、-1、0、正整数。
                - None: 不进行任何提取，直接返回完整数据。
                - -1: 将数据拼接成字符串返回。
                - 0: 随机返回数据中的一个元素。
                - 正整数: 返回数据中指定索引位置的元素，索引从1开始计数。
        
        Returns:
            Union[None, str, List[str]]: 提取后的数据，返回类型根据randoms参数的值而定。
                - 当randoms为None时，返回完整数据，类型为List[str]。
                - 当randoms为-1时，返回拼接后的字符串，类型为str。
                - 当randoms为0时，返回随机选取的一个元素，类型为str。
                - 当randoms为正整数时，返回指定索引位置的元素，类型为str。
                - 当提取失败时，返回None。
        
        """
        if randoms is not None:
            randoms = int(randoms)
        data = self.read(key_name)
        if randoms is not None:
            data_key = {
                randoms: data[int(randoms) - 1] if randoms != -1 else None,
                0: random.choice(data) if data else None,
                -1: ','.join(data) if data else '',
                -2: ','.join(data).split(',') if data else []
            }
            # print(data,'00000')
            data = data_key.get(randoms, data)

        return data

    def md5(self, data):
        return '123456' + data

    def get_date(self):
        return time.strftime("%Y-%m-%d", time.localtime())

    def type_conversion_int(self, data):
        """
        将传入的数据转换为整数类型。
        
        Args:
            data: 待转换的数据，可以为整型、浮点型、布尔型、字符串等。
        
        Returns:
            转换后的整数，如果传入的数据为空，则返回None。
        
        """
        return int(data) if data else None

    def get_date_timestamp(self):
        """
        获取当前时间的 Unix 时间戳（秒级），并返回其字符串形式。
        
        Args:
            无参数。
        
        Returns:
            str: 当前时间的 Unix 时间戳（秒级）的字符串形式。
        
        """
        return str(round(time.time()))


if __name__ == "__main__":
    # DebugTalk().
    pass
