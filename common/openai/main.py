import requests

API_URL = "https://ai.gitee.com/api/endpoints/hacker-zj/whisper-tiny-6705/inference"
headers = {
	"Authorization": "Bearer eyJpc3MiOiJodHRwczovL2FpLmdpdGVlLmNvbSIsInN1YiI6IjM5MDkwIn0.UHEdpOnAye_zLVe3FEV8K5iCjwuK38ptlLQIRdyidY7gIlpdtG3SwkzEpZVus3UUCc-CaSu9BFi4n62Mv_G9DA",
	"Content-Type": "audio/flac"
}

def query(filename):
	with open(filename, "rb") as f:
		data = f.read()
	response = requests.post(API_URL, headers=headers, data=data)
	# print(response.json())
	if response.json():
		return response.json()['text']


output = query("1.weba")
# print(output)