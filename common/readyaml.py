import os

import yaml

from conf.setting import FILE_PATH

def get_data_yaml(files):
    """
    读取YAML文件并返回解析后的数据。
    
    Args:
        files (str): YAML文件路径。
    
    Returns:
        Union[List[List[Dict]], List[Dict]]: 如果YAML文件中只有一个数据块，则返回一个嵌套列表，每个内部列表包含baseInfo和testCase信息；
                                              否则返回整个YAML文件的解析结果。
    
    """
    with open(files, 'r', encoding='utf-8') as f:
        yaml_data = yaml.safe_load(f)
        # return yaml_data
        if len(yaml_data) <=1:
            param = []
            base_info = yaml_data[0].get('baseInfo')
            test_case = yaml_data[0].get('testCase')
            for tc in test_case:
                lists = [base_info, tc]
                param.append(lists)
            return param

        else:
            return yaml_data




    # with open(files, 'r', encoding='utf-8') as f:
    #     yaml_data = yaml.safe_load(f)
    #     return yaml_data

def get_extract_data(key_name):
    """
    根据指定的key_name从提取文件中获取对应的数据
    
    Args:
        key_name (str): 需要获取的key名
    
    Returns:
        Union[Any, None]: 提取文件中key_name对应的数据，若文件不存在则返回None
    
    """
    if os.path.exists(FILE_PATH['extract']):
        with open(FILE_PATH['extract'], 'r', encoding='utf-8') as f:
            data = yaml.safe_load(f)
            # print(data)
            return data[key_name]

    else:
        print('当前无extract文件')
        os.system(FILE_PATH['extract'])




# yaml_data =  get_data_yaml(FILE_PATH['DeleteCart'])
# print(yaml_data)
# print(yaml_data['testCase'][0]['validation'])
# method = yaml_data['baseinfo']['method']
# url = f"http://127.0.0.1:8787{yaml_data['baseinfo']['url']}"
# headers = yaml_data['baseinfo']['headers']
# datalist = yaml_data['baseinfo']['user']
# print(method, url, headers)
# print(type(datalist))
# if __name__ == '__main__':
#     pass
    # for i in datalist:
    #     res = SendRequest().run_main(url=url, method=method, data=i['data'], header=headers)
    #     # print(res.json()['token'])
    #     WriterYaml().write({"token":res.json()['token']})
    # print(get_extract_data('token'))
