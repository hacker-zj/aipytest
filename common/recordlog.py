import os
import logging
import time
from conf import setting
from logging.handlers import RotatingFileHandler

log_path = setting.FILE_PATH['log']
# print(log_path)
if not os.path.exists(log_path):
    os.mkdir(log_path)

log_name = log_path + r'\test.{}.log'.format(time.strftime('%Y%m%d'))


class RecordLog:
    def __init__(self):
        pass

    def output_log(self):
        # 获取当前模块的logger对象
        logger = logging.getLogger(__name__)

        # 如果logger对象没有handler，则进行以下配置
        if not logger.handlers:
            # 设置logger的日志级别
            logger.setLevel(setting.LOG_LEVEL)

            # 设置日志格式
            log_format = logging.Formatter(
                '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)d - %(module)s - %(funcName)s - %(message)s')

            # 创建一个RotatingFileHandler对象，用于写入日志文件
            # 文件名为log_name，模式为追加模式，文件大小达到5MB时进行滚动，最多保留7个备份文件，编码为utf-8
            fh = RotatingFileHandler(filename=log_name, mode='a', maxBytes=5242880, backupCount=7, encoding='utf-8')
            # 设置handler的日志级别
            fh.setLevel(setting.LOG_LEVEL)
            # 设置handler的日志格式
            fh.setFormatter(log_format)
            # 将handler添加到logger对象中
            logger.addHandler(fh)

        # 创建一个StreamHandler对象，用于输出到控制台
        sh = logging.StreamHandler()
        # 设置handler的日志级别
        sh.setLevel(setting.STREAM_LOG_LEVEL)
        # 设置handler的日志格式
        sh.setFormatter(log_format)
        # 将handler添加到logger对象中
        logger.addHandler(sh)

        # 返回配置好的logger对象
        return logger



logs = RecordLog().output_log()