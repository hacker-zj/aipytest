import os

import yaml

from conf.setting import FILE_PATH
class WriterYaml(object):

    def __init__(self):
        pass

    def write(self,value):
        """
        将数据写入指定文件
        
        Args:
            value (dict): 要写入文件的数据，格式为字典
        
        Returns:
            None
        
        """
        files = FILE_PATH['extract']
        if not os.path.exists(files):
            os.system(files)

        try:
            with open(files, 'a', encoding='utf-8') as f:
                if isinstance(value, dict):
                    f.write(yaml.dump(value, allow_unicode=True, sort_keys=True))

                else:
                    print("写入数据必须为dict格式")

        except Exception as e:
            print(e)



