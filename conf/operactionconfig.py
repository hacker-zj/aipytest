import configparser
from .setting import FILE_PATH

class OperactionConfig(object):
    def __init__(self,filepath=None):
        """
        初始化函数，用于读取配置文件
        
        Args:
            filepath (str, optional): 配置文件路径，默认为None。如果为None，则使用FILE_PATH['conf']作为配置文件路径。
        
        Returns:
            None
        
        """
        if filepath is None:
            self.__filepath= FILE_PATH['conf']
        else:
            self.__filepath = filepath

        self.conf = configparser.ConfigParser()
        try:
            self.conf.read(self.__filepath, encoding='utf-8')
        except Exception as e:
            print(e)


    def get_section_for_data(self,section,option):
        """
        从配置文件中读取指定section下的option对应的值
        
        Args:
            section (str): 配置文件中的section名
            option (str): 配置文件中section下对应的option名
        
        Returns:
            Any: option对应的值，类型根据配置文件中的数据类型决定
        
        Raises:
            无，如果出现错误会打印错误信息并返回None
        
        """
        try:
            return self.conf.get(section, option)
        except Exception as e:
            print(f'读取配置文件出错，传入参数是{section,option}')


# if __name__ == '__main__':
#     op = OperactionConfig()
#     print(op.get_section_for_data('MYSQL', 'user'))