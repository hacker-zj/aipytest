import logging
import os
import sys

DIR_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(DIR_PATH)

# log日志输出级别
LOG_LEVEL = logging.DEBUG  # 日志输入到文件
STREAM_LOG_LEVEL = logging.DEBUG  # 日志输入到控制台
MAX_THREAD = 10  #系统最大线程，并发网络请求执行线程数
FAKER_COUNT = 10  #每次模拟生成多少数据

# 文件路径

FILE_PATH = {
    # 'extract': os.path.join(DIR_PATH, 'extract.yaml'),
    # 'login': os.path.join(DIR_PATH, 'testcase', '01_User', '01_登录测试用例.yaml'),
    # 'adduser': os.path.join(DIR_PATH, 'testcase', '01_User', '02_新增用户测试用例.yaml'),
    # 'queryuser': os.path.join(DIR_PATH, 'testcase', '01_User', '05_查询用户测试用例.yaml'),
    'conf': os.path.join(DIR_PATH, 'conf', 'config.ini'),
    'log': os.path.join(DIR_PATH, 'log'),
    'testcase': os.path.join(DIR_PATH, 'testcase')
    # 'good_list': os.path.join(DIR_PATH, 'testcase', '02_ProductController', '01_goodsList.yaml'),
    # 'productDetail': os.path.join(DIR_PATH, 'testcase', '02_ProductController', '02_productDetail.yaml'),
    # 'add_order': os.path.join(DIR_PATH, 'testcase', '02_ProductController', '05_SubmitOrder.yaml'),
    # 'pay_order': os.path.join(DIR_PATH, 'testcase', '02_ProductController', '07_PayOrder.yaml'),
    # 'add_user_template': os.path.join(DIR_PATH, 'mockdata', 'template', 'adduser_mock.yaml')
}


# 遍历根目录及其子目录,自动读取yaml文件
for root, dirs, files in os.walk(DIR_PATH):
    for file in files:
        if file.endswith('.yaml'):
            # 获取文件名（不带扩展名）
            file_key = os.path.splitext(file)[0]
            # 获取相对路径
            relative_path = os.path.join(DIR_PATH, root, file)
            # 替换路径中的反斜杠为正斜杠（兼容Windows）
            relative_path = relative_path.replace('\\', '/')
            # 存储到字典中
            FILE_PATH[file_key] = os.path.join('DIR_PATH', relative_path)

# 打印输出
# print("FILE_PATH = {")
# for key, value in FILE_PATH.items():
#     print(f"    '{key}': {value},")
# print("}")
# if __name__ == '__main__':
#     print(FILE_PATH['conf'])