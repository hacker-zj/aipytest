from faker import Faker
import yaml
from conf.setting import FILE_PATH
import re

fake = Faker('zh-CN')


class CreateMockData(object):
    def __init__(self, file_path, number=100):
        """
        file_path: 模板文件路径，动态数据用 <<>> 括起
        number: 需要生成的数据量
        return: 返回列表格式数据----二维列表
        """
        self.number = number
        self.file_path = file_path
        with open(self.file_path, 'r', encoding='utf-8') as f:
            self.yaml_data = yaml.safe_load(f)[0]

    def replace_placeholder(self, match):
        """
        替换文本中的占位符为模拟数据。
        
        Args:
            match (re.Match): 正则表达式匹配对象，表示匹配到的占位符。
        
        Returns:
            str: 替换后的字符串。
        
        """
        placeholder = match.group(1)

        # 提取函数名和参数
        func_match = re.match(r'(\w+)\((.*)\)', placeholder)
        if func_match:
            func_name = func_match.group(1)
            func_args = func_match.group(2)
            # 提取参数中的字符串和整数
            func_args_list = []
            for arg in func_args.split(','):
                arg = arg.strip()
                if arg.startswith('"') and arg.endswith('"'):
                    func_args_list.append(arg.strip('"'))
                elif arg.isdigit():
                    func_args_list.append(int(arg))
                else:
                    try:
                        func_args_list.append(float(arg))
                    except ValueError:
                        pass

            return str(getattr(fake, func_name)(*func_args_list))
        return placeholder

    def create_mock_data(self):
        """
        根据yaml文件中的测试数据模板生成mock数据。
        
        Args:
            无参数。
        
        Returns:
            list: 由包含baseInfo和testCase的字典列表组成的列表，即二维列表。
        
        """
        data = []
        base_info = self.yaml_data['baseInfo']
        test_case = self.yaml_data['testCase'][0]
        # test_case = BaseRequest().replace_load(test_case)
        for i in range(0, self.number):
            pattern = re.compile(r'<<(.+?)>>')
            result_str = pattern.sub(self.replace_placeholder, str(test_case))
            # 将字符串转换回字典
            result_data = eval(result_str)
            case_list = [base_info, result_data]
            data.append(case_list)
        return data
        # print(result_data)

if __name__ == '__main__':
    print(CreateMockData(FILE_PATH['productDetail_mock'], 1).create_mock_data())
#     pass
#     print(CreateAdduserData(FILE_PATH['add_user_template'], 2).create_user_data())
