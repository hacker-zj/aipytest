# test_cases.py
import pytest
import allure
from base.apiutil import BaseRequest  # 替换为你的实际模块路径


@pytest.mark.usefixtures("baseinfo", "testcase", "parent_folder", "file_name")
def test_GetMateriel(baseinfo, testcase, parent_folder, file_name):
    allure.dynamic.feature(parent_folder)
    allure.dynamic.story(file_name)
    allure.dynamic.title(testcase['case_name'])
    BaseRequest().case_yaml(baseinfo, testcase)
