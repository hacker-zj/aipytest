import os
import subprocess
import shutil
import pytest


def copy_environment_xml():
    source = './environment.xml'
    destination = './allure-results/environment.xml'

    # 确保目标目录存在
    os.makedirs(os.path.dirname(destination), exist_ok=True)

    # 复制文件
    shutil.copyfile(source, destination)


def run_tests():
    # 删除旧的 Allure 结果目录
    if os.path.exists('./allure-results'):
        shutil.rmtree('./allure-results')

    # 创建新的 Allure 结果目录
    os.makedirs('./allure-results', exist_ok=True)

    # 复制 environment.xml 文件
    copy_environment_xml()

    # 运行 pytest 并生成 Allure 结果
    pytest.main(['--junitxml=./report/results.xml'])

    # 生成 Allure 报告
    subprocess.run(['allure', 'generate', './allure-results', '-o', './allure-report', '--clean'], shell=True)

    # 打开 Allure 报告
    subprocess.run(['allure', 'open', './allure-report'], shell=True)


if __name__ == '__main__':
    run_tests()
