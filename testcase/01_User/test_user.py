import pytest
import allure
from common.readyaml import get_data_yaml
from conf.setting import FILE_PATH
from base.apiutil import BaseRequest
from common.createid import c_id, m_id


@pytest.mark.run(order=1)
@allure.feature(next(m_id) + "用户管理测试用例")
class TestUser:

    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['login']))
    @allure.story(next(c_id) + "登录测试用例")
    def test_login(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + "新增用户测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['adduser']))
    def test_adduser(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + "删除用户测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['deleteuser']))
    def test_deleteuser(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + "修改用户测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['updateuser']))
    def test_updateuser(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + "查询用户测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['queryuser']))
    def test_queryuser(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)
