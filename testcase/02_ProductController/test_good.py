import pytest
from conf.setting import FILE_PATH
from common.readyaml import get_data_yaml
from base.apiutil import BaseRequest
import allure
from common.createid import m_id, c_id


@pytest.mark.run(order=2)
@allure.feature(next(m_id) + '商品管理接口')
class TestGood(object):
    @allure.story(next(c_id) + '商品列表测试用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['goodsList']))
    def test_good_list(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '商品详情测试用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['productDetail']))
    def test_productDetail(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '添加购物车测试用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['AddCart']))
    def test_AddCart(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '删除购物车测试用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['DeleteCart']))
    def test_DeleteCart(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '订单提交测试用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['SubmitOrder']))
    def test_SubmitOrder(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '校验商品库存用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['CheckGoodInventory']))
    def test_CheckGoodInventory(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '订单支付测试用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['PayOrder']))
    def test_PayOrder(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '校验商品订单状态用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['CheckGoods']))
    def test_CheckGoods(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + '校验商品物流状态用例')
    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['CheckLogistics']))
    def test_CheckLogistics(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)