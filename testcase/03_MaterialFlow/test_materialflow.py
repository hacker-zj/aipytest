import pytest
import allure
from common.readyaml import get_data_yaml
from conf.setting import FILE_PATH
from base.apiutil import BaseRequest
from common.createid import c_id, m_id


@pytest.mark.run(order=5)
@allure.feature(next(m_id) + "物流管理测试用例")
class TestMater:

    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['GetMateriel']))
    @allure.story(next(c_id) + "获取下单物料信息用例")
    def test_GetMateriel(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['MerchantOrder']))
    @allure.story(next(c_id) + "货主（托运人）下订单用例")
    def test_MerchantOrder(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['AcceptOrder']))
    @allure.story(next(c_id) + "集团接收货主订单用例")
    def test_AcceptOrder(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['LogisticsOrder']))
    @allure.story(next(c_id) + "集团分配订单给物流公司")
    def test_LogisticsOrder(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)

    @pytest.mark.parametrize('baseinfo,testcase', get_data_yaml(FILE_PATH['LogisticsOrderReceiving']))
    @allure.story(next(c_id) + "物流公司接单")
    def test_LogisticsOrderReceiving(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'])
        BaseRequest().case_yaml(baseinfo, testcase)