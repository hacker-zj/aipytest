import pytest
import allure
from common.readyaml import get_data_yaml
from conf.setting import FILE_PATH, MAX_THREAD, FAKER_COUNT
from base.apiutil import BaseRequest
from mockdata.createdata import CreateMockData
from common.createid import c_id, m_id
from concurrent.futures import ThreadPoolExecutor, as_completed

create_add_user = CreateMockData(FILE_PATH['adduser_mock'], FAKER_COUNT).create_mock_data()
productDetail_mock = CreateMockData(FILE_PATH['productDetail_mock'], FAKER_COUNT).create_mock_data()
Addcart_mock = CreateMockData(FILE_PATH['Addcart_mock'], FAKER_COUNT).create_mock_data()


@pytest.mark.run(order=3)
@allure.feature(next(m_id) + "多任务并发测试用例")
class TestUserMultiTask:
    def send_request(self, baseinfo, testcase):
        BaseRequest().case_yaml(baseinfo, testcase)

    @allure.story(next(c_id) + "多用户模拟注册测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', create_add_user)
    def test_user_register_moretask(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'] + '_' + testcase['data']['username'])
        # 使用线程池来实现并发请求
        with ThreadPoolExecutor(max_workers=MAX_THREAD) as executor:
            futures = [executor.submit(self.send_request, baseinfo, testcase)]
            for future in as_completed(futures):
                try:
                    future.result()
                except Exception as e:
                    print(f"Request failed: {e}")

    @allure.story(next(c_id) + "多用户模拟查看商品详情测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', productDetail_mock)
    def test_productDetail_mock(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'] + '_' + testcase['data']['phone'])
        # 使用线程池来实现并发请求
        with ThreadPoolExecutor(max_workers=MAX_THREAD) as executor:
            futures = [executor.submit(self.send_request, baseinfo, testcase)]
            for future in as_completed(futures):
                try:
                    future.result()
                except Exception as e:
                    print(f"Request failed: {e}")

    @allure.story(next(c_id) + "多用户模拟商品加入购物车测试用例")
    @pytest.mark.parametrize('baseinfo,testcase', Addcart_mock)
    def test_Addcart_mock(self, baseinfo, testcase):
        allure.dynamic.title(testcase['case_name'] + '_' + testcase['data']['phone'])
        # 使用线程池来实现并发请求
        with ThreadPoolExecutor(max_workers=MAX_THREAD) as executor:
            futures = [executor.submit(self.send_request, baseinfo, testcase)]
            for future in as_completed(futures):
                try:
                    future.result()
                except Exception as e:
                    print(f"Request failed: {e}")
